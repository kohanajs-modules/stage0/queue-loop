const { build } = require('kohanajs-start');
const path = require('path')

build(
  `${__dirname}/queue.graphql`,
  `${__dirname}/queue.js`,
  `${__dirname}/exports/queue.sql`,
  `${__dirname}/default/database/queue.sqlite`,
  `${__dirname}/exports/model/queue`,
  true
);
