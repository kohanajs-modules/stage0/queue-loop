const {ORM} = require('kohanajs');

class QueueState extends ORM{
  name = null;

  static joinTablePrefix = 'queue_state';
  static tableName = 'queue_states';

  static fields = new Map([
    ["name", "String"]
  ]);
  static hasMany = [
    ["queue_state_id", "Queue"]
  ];
}

module.exports = QueueState;
